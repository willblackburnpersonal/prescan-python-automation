import json
import subprocess

# execution variables
#############################
cli_exe = "cmd_line_test.exe"
cli_path = "scratch/"
auto_file = "scratch/json"
#############################

# open json file and populate a dict list with contents
with open("scratch/json") as json_file:
    json_contents = json.load(json_file)

# iterate over experiment runs loaded from json
for exp_run in json_contents:

    # configure experiment here including calls to prescan
    exp_name = exp_run.get('experiment')

    # check if experiment defined
    if exp_name is not None:
        exp_vars = exp_run.get('variables')

        # check if vars defined
        if exp_vars is not None:

            # extract variables from current exp run and join them into a CLI argument string
            cli_vars = ["-set %s=%s" % (var, str(exp_vars.get(var))) for var in exp_vars]
            cli_args = " ".join(cli_vars) + " "

        else:
            cli_args = ""

        # form actual CLI argument string with non-variable args if any
        cli_cmd = "-load %s-build -save -exit" % cli_args

        # send run to prescan using API and dict keys in run
        # wait for experiment to complete
        # maybe do some result stuff?prescan_exp_automate.py

        # run dummy exe to test CLI call and passing of args
        print "%s ran" % exp_name
        subprocess.call([cli_path + cli_exe, cli_cmd])

    else:
        print "No experiment given!"
