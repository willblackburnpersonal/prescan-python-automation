import ctypes

# load the dll and the Sum function, define arg and return prototypes
loaded_dll = ctypes.CDLL("dummy.dll")
loaded_dll.Sum.argtypes = [ctypes.c_double, ctypes.c_double]
loaded_dll.Sum.restype = ctypes.c_double


# create python function to simplify call to dll
def calc_sum(num1=0.0, num2=0.0):
    return loaded_dll.Sum(num1, num2)


# numbers to sum using dll
num1 = 10.0
num2 = 14.0

# do addition
print calc_sum(num1, num2)
