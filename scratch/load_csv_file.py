import csv

# open csv file and populate a dict list with row entries and col/row
# headers as dict keys
with open('csv') as csv_file:
    csv_entries = list(csv.DictReader(csv_file))

# iterate over experiment runs loaded from csv
for row in csv_entries:
    print row
